# Example Resin docker-compose multicontainer project
## Abstract
Fast and easy multi container testing using a MQTT broker, a PostgeSQL DB and a Python app sending fake temperature data every 5 seconds. 

## Details
Connecting from host to the Mosquitto container can be done via _localhost:1883_. Containers use the bridge network to communicate with each other. Services use the name of the service they want to connect to:

    client = mqtt.Client()
    client.connect("mosquitto", 1883)

The name of the service is also the hostname of the container.

## Architecture AArch64 vs X64
So to run the multi conatiner project locally, images pulled by docker must be built for X64 architecture. For Resin and a Raspberry Pi scenario images for a different arch have to be pulled. Hence, there are two docker-compose and two Docker files. The one used for local usage is labeled with _-x64_.

## push to resin repo
git push resin master

## Test locally
docker-compose up -d

## Postgres
Acts as data storage. Gets pulled from online repo.

## MQTT
Message broker. Gets pulled from online repo.

## App
Example app which generates fake temperature values and publishes them to Mosquitto and stores them in DB. The App gets built locally.
