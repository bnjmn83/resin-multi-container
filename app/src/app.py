import os
import sys
import os
import sys
import logging
import time
import random
import json
import postgres_wrapper as pgwrap
from datetime import datetime
from time import sleep
import paho.mqtt.client as mqtt
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

BROKER = os.environ.get('BROKER', "localhost")
BROKER_PORT = os.environ.get('BROKER_PORT', 1883)
BROKER_REPORT_TOPIC = os.environ.get('BROKER_REPORT_TOPIC', "device/reports")

POSTGRES_HOST = os.environ.get('POSTGRES_HOST', "localhost")
POSTGRES_PORT = os.environ.get('POSTGRES_PORT', 5432)
POSTGRES_USER = os.environ.get('POSTGRES_USER', "postgres")
POSTGRES_PW = os.environ.get('POSTGRES_PW', "mysecretpassword")

TABLE_NAME = os.environ.get('TABLE_NAME', 'reports')
TABLE_COLUMNS = "log_time timestamp, json_obj JSONB"

DEBUG = os.environ.get('DEBUG', 0)

# Postrges wrapper instance
postgres = pgwrap.PostgresWrapper(
    user=POSTGRES_USER, host=POSTGRES_HOST, password=POSTGRES_PW)

def update_edge_reports(data):
    """ Update DB with latest temp readings"""
    date = datetime.now().isoformat(timespec='seconds', sep=' ')
    logging.debug(f"Insert into DB {date}")
    postgres.insert_into_table(TABLE_NAME, data, date)

def main():

    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    while True:
        try:
            postgres.connect_db()
            break
        except:
            time.sleep(5)
            
    try:
        postgres.create_table(TABLE_NAME, TABLE_COLUMNS)
    except:
        sys.exit(2)

    logging.info(f"MQTT connecting to {BROKER} {BROKER_PORT}")

    client = mqtt.Client()
    client.on_connect = lambda client, userdata, rc: print("Connected to broker")
    client.connect(BROKER, int(BROKER_PORT))
    client.loop_start()
    
    while True:
        time.sleep(5)
        data = {"temperature" : random.randint(1,100)}
        logging.info(f"{datetime.now().isoformat(timespec='seconds', sep=' ')} {data}")
        client.publish(BROKER_REPORT_TOPIC, json.dumps(data))
        update_edge_reports(data)

if __name__ == '__main__':
    main()