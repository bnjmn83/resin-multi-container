import os
import sys
import logging
import time
import json
from datetime import datetime
from inspect import currentframe
from time import sleep
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

def get_linenumber():
    """ Debug helper """
    return " In Line: {}".format(currentframe().f_back.f_lineno)

class PostgresWrapper:
    """ A simple wrapper class """
    conn = None
    cursor = None
    password = None
    user = None
    host = None

    def __init__(
            self,
            user="postgres",
            host="localhost",
            password="mysecretpassword"):

        self.password = password
        self.host = host
        self.user = user

    def connect_db(self, db_name="postgres"):
        """ Connect to DB """
        if self.conn is not None:
            self.conn.close()

        logging.info("Connecting to {} {} {} {}.".format(self.host, self.user, self.password , db_name))

        try:
            self.conn = psycopg2.connect(dbname=db_name, user=self.user, password=self.password, host=self.host)
        except Exception as x:
            print(x, get_linenumber())
            self.conn = None
            raise

        self.conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        self.cursor = self.conn.cursor()
        logging.info("Connect to DB {} successfull.".format(db_name))
        return True

    def close(self):
        self.cursor.close()
        self.conn.close()

    def get_cursor(self):
        return self.cursor

    def list_dbs(self):
        self.cursor.execute(
            "select relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';"
        )
        dbs = self.cursor.fetchall()
        print (dbs)
        return dbs

    def table_exists(self, table_name):
        self.cursor.execute(
            "select exists(select * from information_schema.tables where table_name={})".format(table_name))
        return self.cursor.fetchone()[0]

    def create_db(self, db_name):
        # First try if the DB does not already exist
        if self.connect_db(db_name):
            print("DB already exists")
            return True

        # Connect to default DB
        if not self.connect_db():
            print("Fatal")
            sys.exit(3)

        # Create new DB
        try:
            self.cursor.execute("CREATE DATABASE %s;" % db_name)
        except Exception as x:
            print(x, get_linenumber())
            return False

        print("Create DB {}".format(db_name))
        return True

    def execute_query(self, query_string):
        try:
            self.cursor.execute(query_string)
        except Exception as x:
            print(x, get_linenumber())
            return False

    def create_table(self, table_name, table_columns):
        print("Creating table {} {}.".format(table_name,table_columns))
        
        try:
            self.cursor.execute(
                "CREATE TABLE IF NOT EXISTS {} ( id SERIAL PRIMARY KEY, {});".
                format(table_name, table_columns))
            
        except Exception as x:
            print(x, get_linenumber())
            raise

        logging.info("Created table {} successfully.".format(table_name))

    def insert_into_table(self, table_name, json_obj, date):
        try:
            query_str = "insert into {} (log_time, json_obj) VALUES ('{}', '{}')".format(
                table_name, date, json.dumps(json_obj))
            self.cursor.execute(query_str)
            print (query_str)
            sys.stdout.flush()
            return True
        except Exception as x:
            print(x, get_linenumber())
            return False